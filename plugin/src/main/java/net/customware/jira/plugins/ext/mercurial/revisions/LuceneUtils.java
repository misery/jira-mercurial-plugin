/*
 * Copied here because of difficulities getting OSGI dependency injection working.
 */
package net.customware.jira.plugins.ext.mercurial.revisions;

import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexManager;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * A simple utility class for our common Lucene usage methods.
 */
public class LuceneUtils
{
    private static Log log = LogFactory.getLog(LuceneUtils.class);

    private static final String LOCK_FILENAME_PREFIX = "Lock@";

    private static final DateFormatter dateFormatter = new DateFormatter();

    public static IndexReader getIndexReader(final String path) throws IndexException
    {
        try
        {
            return IndexReader.open(FSDirectory.open(new File(path)));
        }
        catch (final IOException e)
        {
            log.error("Problem with path " + path + ": " + e.getMessage(), e);
            throw new IndexException("Problem with path " + path + ": " + e.getMessage(), e);
        }
    }

    public static IndexWriter getIndexWriter(final String path, final boolean create, final StandardAnalyzer analyzer) throws IndexException
    {
        try
        {
            createDirRobust(path);

			IndexWriterConfig indexWriterConfig = new IndexWriterConfig(IssueIndexManager.LUCENE_VERSION, analyzer);
            final IndexWriter indexWriter = new IndexWriter(FSDirectory.open(new File(path)), indexWriterConfig);
            indexWriter.setUseCompoundFile(true);
            return indexWriter;
        }
        catch (final IOException e)
        {
            log.error("Problem with path " + path + ": " + e.getMessage(), e);
            throw new IndexException("Problem with path " + path + ": " + e.getMessage(), e);
        }
    }

    /**
     * Create a directory (robustly) or throw appropriate Exception
     *
     * @param path Lucene index directory path
     * @throws IOException if cannot create directory, write to the directory, or not a directory
     */
    private static void createDirRobust(final String path) throws IOException
    {
        final File potentialPath = new File(path);
        if (!potentialPath.exists())
        {
            log.warn("Directory " + path + " does not exist - perhaps it was deleted?  Creating..");

            final boolean created = potentialPath.mkdirs();
            if (!created)
            {
                log.warn("Directory " + path + " could not be created.  Aborting index creation");
                throw new IOException("Could not create directory: " + path);
            }
        }
        if (!potentialPath.isDirectory())
        {
            log.warn("File " + path + " is not a directory.  Cannot create index");
            throw new IOException("File " + path + " is not a directory.  Cannot create index");
        }
        if (!potentialPath.canWrite())
        {
            log.warn("Dir " + path + " is not writable.  Cannot create index");
            throw new IOException("Dir " + path + " is not writable.  Cannot create index");
        }
    }

    /**
     * Given a {@link Collection} of paths that represent index directories checks if there are any existing
     * Lucene lock files for the passed paths. This method returns a {@link Collection} of file paths of any existing
     * Lucene lock files. If no lock files are found an empty collection is returned.
     * <p/>
     * A common usage of this methdo would be:
     * <pre>
     * Collection existingLockFilepaths = LuceneUtils.getStaleLockPaths(indexManager.getAllIndexPaths());
     * </pre>
     * </p>
     *
     * @param indexDirectoryPaths collection of index directory paths
     * @return collection of file paths of any existing Lucene lock files
     */
    public static Collection<String> getStaleLockPaths(final Collection<String> indexDirectoryPaths)
    {
        // A collection to which we will add all found lock file paths (if any)
        final Collection<String> existingLockFilepaths = new ArrayList<String>();

        try
        {
            // Get a path for each index directory
            if (indexDirectoryPaths != null)
            {
                for (final String indexDirectoryPath : indexDirectoryPaths)
                {
                    existingLockFilepaths.addAll(getLocks(indexDirectoryPath));
                }
            }
        }
        catch (final IOException e)
        {
            log.error("While trying to check for stale lock files: " + e.getMessage());
        }

        return existingLockFilepaths;
    }

    public static String dateToString(final Date date)
    {
        return dateFormatter.dateToString(date, Resolution.SECOND);
    }

    public static Date stringToDate(final String s)
    {
        if (!StringUtils.isBlank(s))
        {
            return dateFormatter.stringToDate(s);
        }
        return new Date();
    }

    private static Collection<String> getLocks(final String path) throws IOException
    {
        final Collection<String> locks = new ArrayList<String>();

        Directory dir = null;
        try
        {
        	dir = FSDirectory.open(new File(path));
            // Check write lock
            final org.apache.lucene.store.Lock lock = dir.makeLock(IndexWriter.WRITE_LOCK_NAME);
            if (lock.isLocked())
            {
                locks.add(getLockFilepath(lock));
            }
        }
        finally
        {
            if (dir != null)
            {
                dir.close();
            }
        }

        return locks;
    }

    private static String getLockFilepath(final org.apache.lucene.store.Lock lock)
    {
        if (lock == null)
        {
            return "";
        }

        String filePath = lock.toString();
        if ((filePath != null) && filePath.startsWith(LOCK_FILENAME_PREFIX))
        {
            filePath = filePath.substring(LOCK_FILENAME_PREFIX.length());
        }

        return filePath;
    }

    /**
     * do not ctor
     */
    private LuceneUtils()
    {}

    static class DateFormatter
    {
        private final DateTimeFormatter year;
        private final DateTimeFormatter month;
        private final DateTimeFormatter day;
        private final DateTimeFormatter hour;
        private final DateTimeFormatter minute;
        private final DateTimeFormatter second;
        private final DateTimeFormatter millisecond;

        DateFormatter()
        {
            final DateTimeFormatterBuilder builder = new DateTimeFormatterBuilder()
            {
                @Override
                public DateTimeFormatter toFormatter()
                {
                    return super.toFormatter().withZone(DateTimeZone.UTC);
                }
            };
            year = builder.appendYear(4, 4).toFormatter();
            month = builder.appendMonthOfYear(2).toFormatter();
            day = builder.appendDayOfMonth(2).toFormatter();
            hour = builder.appendHourOfDay(2).toFormatter();
            minute = builder.appendMinuteOfHour(2).toFormatter();
            second = builder.appendSecondOfMinute(2).toFormatter();
            millisecond = builder.appendMillisOfSecond(3).toFormatter();
        }

        /**
         * Converts a string produced by <code>timeToString</code> or
         * <code>dateToString</code> back to a time, represented as a
         * Date object.
         *
         * @param dateString the date string to be converted
         * @return the parsed time as a Date object
         * @throws DateParsingException if <code>dateString</code> is not in the
         *  expected format
         */
        public Date stringToDate(final String dateString) throws DateParsingException
        {
            return stringToDateTime(dateString.trim()).toDate();
        }

        /**
         * Converts a string produced by <code>timeToString</code> or
         * <code>dateToString</code> back to a time, represented as a
         * Date object.
         *
         * @param dateString the date string to be converted
         * @return the parsed time as a Date object
         * @throws DateParsingException if <code>dateString</code> is not in the
         *  expected format
         */
        public DateTime stringToDateTime(final String dateString) throws DateParsingException
        {
            switch (dateString.length())
            {
                case 4:
                    return year.parseDateTime(dateString);
                case 6:
                    return month.parseDateTime(dateString);
                case 8:
                    return day.parseDateTime(dateString);
                case 10:
                    return hour.parseDateTime(dateString);
                case 12:
                    return minute.parseDateTime(dateString);
                case 14:
                    return second.parseDateTime(dateString);
                case 17:
                    return millisecond.parseDateTime(dateString);

                default:
                    throw new DateParsingException(dateString);
            }
        }

        /**
         * Converts a Date to a string suitable for indexing.
         *
         * @param date the date to be converted
         * @param resolution the desired resolution, see
         *  {@link Resolution}
         * @return a string in format <code>yyyyMMddHHmmssSSS</code> or shorter,
         *  depending on <code>resolution</code>; using GMT as timezone
         */
        public String dateToString(final Date date, final Resolution resolution)
        {
            return timeToString(date.getTime(), resolution);
        }

        /**
         * Converts a millisecond time to a string suitable for indexing.
         *
         * @param date the date expressed as milliseconds since January 1, 1970, 00:00:00 GMT
         * @param resolution the desired resolution, see
         *  {@link Resolution}
         * @return a string in format <code>yyyyMMddHHmmssSSS</code> or shorter,
         *  depending on <code>resolution</code>; using GMT as timezone
         */
        public String timeToString(final long date, final Resolution resolution)
        {
            switch (resolution)
            {
                case YEAR:
                    return year.print(date);
                case MONTH:
                    return month.print(date);
                case DAY:
                    return day.print(date);
                case HOUR:
                    return hour.print(date);
                case MINUTE:
                    return minute.print(date);
                case SECOND:
                    return second.print(date);
                case MILLISECOND:
                    return millisecond.print(date);
            }
            throw new IllegalArgumentException("unknown resolution " + resolution);
        }
    }

    static class DateParsingException extends RuntimeException
    {
        public DateParsingException(final String dateString)
        {
            super("Input is not valid date string: " + dateString);
        }
    }

    private static enum Resolution
    {
        YEAR,
        MONTH,
        DAY,
        HOUR,
        MINUTE,
        SECOND,
        MILLISECOND
    }
}
