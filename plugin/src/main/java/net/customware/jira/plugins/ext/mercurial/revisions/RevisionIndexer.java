/*
 * Created by IntelliJ IDEA.
 * User: Mike
 * Date: Oct 1, 2004
 * Time: 4:58:40 PM
 */
package net.customware.jira.plugins.ext.mercurial.revisions;

import net.customware.jira.plugins.ext.mercurial.MultipleMercurialRepositoryManager;
import net.customware.jira.plugins.ext.mercurial.MercurialManager;
import net.customware.hg.core.io.HGException;
import net.customware.hg.core.io.HGLogEntry;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.ManagerFactory;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.InfrastructureException;
import com.atlassian.jira.config.util.IndexPathManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.service.ServiceManager;
import com.atlassian.jira.util.JiraKeyUtils;

import com.opensymphony.util.TextUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.DateField;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.regex.*;

public class RevisionIndexer
{
    private static Logger log = Logger.getLogger(RevisionIndexer.class);
    private static final Long NOT_INDEXED = -1L;

    // Explicitly allow references to projects that never existed or
    // no longer exist in JIRA to be retrieved from the index
    private boolean requireValidProject = false;

    static final String REVISIONS_INDEX_DIRECTORY = "atlassian-mercurial-revisions";

    // These are names of the fields in the Lucene documents that contain revision info.
    public static final String FIELD_REVISIONNUMBER = "revision";
    public static final Term START_REVISION = new Term(FIELD_REVISIONNUMBER, "");
    public static final String FIELD_MESSAGE = "message"; // the whole message
    public static final String FIELD_AUTHOR = "author";
    public static final String FIELD_TAG = "tag";
    public static final String FIELD_DATE = "date";
    public static final String FIELD_ISSUEKEY = "key";
    public static final String FIELD_PROJECTKEY = "project";
    public static final String FIELD_REPOSITORY = "repository";

    public static final StandardAnalyzer ANALYZER = new StandardAnalyzer(IssueIndexManager.LUCENE_VERSION);

    public static final int MAX_REVISIONS = 100;

    private final MultipleMercurialRepositoryManager multipleMercurialRepositoryManager;
    private final VersionManager versionManager;
    private final IssueManager issueManager;
    private final PermissionManager permissionManager;
    private final ProjectManager projectManager;
    private final ChangeHistoryManager changeHistoryManager;
    private final ServiceManager serviceManager;
    private final IndexPathManager indexPathManager;
    private Hashtable<Long, Long> latestIndexedRevisionTbl;
    private DefaultLuceneIndexAccessor indexAccessor;

    public RevisionIndexer(MultipleMercurialRepositoryManager multipleMercurialRepositoryManager)
    {
        this.multipleMercurialRepositoryManager = multipleMercurialRepositoryManager;
        this.versionManager = ComponentManager.getInstance().getVersionManager();
        this.issueManager = ComponentManager.getInstance().getIssueManager();
        this.permissionManager = ComponentManager.getInstance().getPermissionManager();
        this.projectManager = ComponentManager.getInstance().getProjectManager();
        this.changeHistoryManager = ComponentManager.getInstance().getChangeHistoryManager();
        this.serviceManager = ManagerFactory.getServiceManager();
        this.indexPathManager = ComponentManager.getInstance().getIndexPathManager();

        this.indexAccessor = new DefaultLuceneIndexAccessor();
        initializeLatestIndexedRevisionCache();
    }

    public void start()
    {
        try
        {
            createIndexIfNeeded();
            RevisionIndexService.install(serviceManager); // ensure the changes index service is installed
        }
        catch (Exception e)
        {
            log.error("Error installing the revision index service.", e);
            throw new InfrastructureException("Error installing the revision index service.", e);
        }
    }

    /**
     * Looks for the revision index directory and creates it if it does not already exists.
     *
     * @return Return <tt>true</tt> if the index directory is usable or created; <tt>false</tt> otherwise.
     */
    private boolean createIndexIfNeeded()
    {
        if (log.isDebugEnabled())
            log.debug("RevisionIndexer.createIndexIfNeeded()");

        boolean indexExists = indexDirectoryExists();
        if (getIndexPath() != null && !indexExists)
        {
            IndexWriter indexWriter = null;
            try 
            {
                indexWriter = indexAccessor.getIndexWriter(getIndexPath(), true, ANALYZER);
                initializeLatestIndexedRevisionCache();
                return true;
            }
            catch (IndexException e)
            {
                log.error("There's a problem initializing the index.", e);
                return false;
            }
            finally
            {
            try 
            {
              if(indexWriter != null)
              {
                indexWriter.close();
              }
            } 
            catch (IOException ioe)
            {
                log.error("There's a problem performing IO on the index.", ioe);
                return false;
            }
            }
        }
        else
        {
            return indexExists;
        }
    }

    private void initializeLatestIndexedRevisionCache()
    {
        Collection<MercurialManager> repositories = multipleMercurialRepositoryManager.getRepositoryList();

        latestIndexedRevisionTbl = new Hashtable<Long, Long>();

        for (MercurialManager currentRepo : repositories) {
            initializeLatestIndexedRevisionCache(currentRepo);
        }
        log.info("Number of Mercurial repositories: " + repositories.size());
    }

    private void initializeLatestIndexedRevisionCache(MercurialManager mercurialManager)
    {
        latestIndexedRevisionTbl.put(mercurialManager.getId(), NOT_INDEXED);
    }

    private boolean indexDirectoryExists()
    {
        try
        {
            // check if the directory exists
            File file = new File(getIndexPath());

            return file.exists();
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public String getIndexPath()
    {
        String indexPath = null;
        String rootIndexPath = indexPathManager.getPluginIndexRootPath();
        // Handle the case where the jira.home is a link
        try {
            rootIndexPath = new File(rootIndexPath).getCanonicalPath();
        } catch (IOException ioe) {
            log.error(ioe.getMessage());
            return indexPath;
        }
        // May also be using a non-default location
        
        if (rootIndexPath != null)
        {
            indexPath = rootIndexPath + System.getProperty("file.separator") + REVISIONS_INDEX_DIRECTORY;
        }
        else
        {
            // TODO deleting the INDEX_DIRECTORY causes the next start
            // up to fail due to no segments, so just delete the
            // segments files if you want to clear an index or the
            // whole plugins directory seems to work too
            log.warn("At the moment the root index path of jira is not set, so we can not form an index path for the mercurial plugin.");
        }

        return indexPath;
    }

    /**
     * This method updates the index, creating it if it does not already exist.
     *
     * @throws IndexException if there is some problem in the indexing subsystem meaning indexes cannot be updated.
     */
    public void updateIndex() throws IndexException
    {
        if (!createIndexIfNeeded()) {
            return;
        }

        Collection<MercurialManager> repositories = multipleMercurialRepositoryManager.getRepositoryList();

        log.info("Number of repositories: " + repositories.size());
        
        MercurialManager tmpRef = null;
        try {
            // There is a small risk of concurrent modification here so
            // we catch it below and ignore it
            for (MercurialManager mercurialManager : repositories) {
                tmpRef = mercurialManager; // just for the exceptions
                checkAndUpdateOneIndex(mercurialManager);
            }
        } catch (ConcurrentModificationException e) {
            log.info("Not indexing during clone or pull operations");
        } catch (IOException e) {
            log.warn("Unable to index repository '" + tmpRef.getDisplayName() + "'", e);
        } catch (RuntimeException e) {
            log.warn("Unable to index repository '" + tmpRef.getDisplayName() + "'", e);
        }        
    }
    
    public void checkAndUpdateOneIndex(MercurialManager mercurialManager) throws IndexException, IOException, ConcurrentModificationException {
    	// HengHwa - JIRA 5.x
    	// JHG-25 - the Plugin nver created the needed index repositories and thus is unable to find the index directory, when creating a new repository
    	createIndexIfNeeded();
    	
        // If the repository isn't active, try activating
        // it. If it still not accessible, skip it
        if (!mercurialManager.isActive()) {
            mercurialManager.activate();
            
            if (!mercurialManager.isActive()) {
                return;
            }
        }
        
        if (mercurialManager.isSubrepos()) {
            // Refresh the list of repositories and move on
            multipleMercurialRepositoryManager.checkSubrepos(mercurialManager);
            return;
        }
        
        long repoId = mercurialManager.getId();
        String repoName = "'" + mercurialManager.getDisplayName() + "'";
        long latestIndexedRevision = -1;
        
        // Retrieve from the cache
        if (getLatestIndexedRevision(repoId) == null) {
            // No latestIndexedRevision, so no need to update. This
            // probably means that the repository has been removed
            // from the file system or hasn't been fully cloned yet.
            log.warn("Did not update index for " + mercurialManager.getDisplayName() + " because not fully cloned yet"); // because null value in hash table for " + repoName);
            return;
        }
        latestIndexedRevision = getLatestIndexedRevision(repoId);
        
        log.info("Checking for new changesets in the repository " + repoName);
        
        if (latestIndexedRevision < 0) {
            // Find the latest revision by looking in the index
            latestIndexedRevision = updateLastRevisionIndexed(repoId);
        }
        
        log.debug("Latest indexed revision for repository " + repoName + " is " + latestIndexedRevision);
        
        updateOneIndex(mercurialManager, latestIndexedRevision);
    }

    /**
     * This calls getLogEntries() which calls hg pull and then updates
     * the index and also cached latest revision if necessary.
     */
    public void updateOneIndex(MercurialManager mercurialManager, long latestIndexedRevision) throws IndexException, IOException, ConcurrentModificationException 
    {
        long repoId = mercurialManager.getId();
        String repoName = "'" + mercurialManager.getDisplayName() + "'";
        // This calls hg pull
        @SuppressWarnings("unchecked")
        final Collection<HGLogEntry> logEntries = mercurialManager.getLogEntries(latestIndexedRevision);
        if (logEntries.size() == 0) {
            return;
        }

        log.info("Updating the JIRA index for the repository " + repoName);

        IndexWriter writer = indexAccessor.getIndexWriter(getIndexPath(), false, ANALYZER);
        
        try {
            final IndexReader reader = indexAccessor.getIndexReader(getIndexPath());
            
            try {
                for (HGLogEntry logEntry : logEntries) {
                    if (TextUtils.stringSet(logEntry.getMessage()) && isKeyInString(logEntry)) {
                        if (!hasDocument(repoId, logEntry.getShortRevision(), reader)) {
                            Document doc = getDocument(repoId, logEntry);
                            log.debug("Indexing repository=" + repoName + ", revision: " + logEntry.getShortRevision());
                            writer.addDocument(doc);
                            if (logEntry.getShortRevision() > latestIndexedRevision) {
                                latestIndexedRevision = logEntry.getShortRevision();
                                // update the in-memory cache issue SVN-71
                                latestIndexedRevisionTbl.put(repoId, latestIndexedRevision);
                            }
                        }
                    } // if
                } // for
            } finally {
                reader.close();
            }
        } finally {
            writer.close();
        }
    }

    /**
     * @return true if there is a string matching the JIRA project key pattern
     * as defined in jira-application.properties
     */
    protected boolean isKeyInString(HGLogEntry logEntry)
    {
        // If the message is used to detect releases, then we
        // have to index all changesets, not just the ones with issue ids
        boolean indexAllMessages = true;
        if (indexAllMessages) {
            return true;
        }
        final String logMessageUpperCase = StringUtils.upperCase(logEntry.getMessage());
        return JiraKeyUtils.isKeyInString(logMessageUpperCase);
    }

    public Long getLatestIndexedRevision(long repoId)
    {
        return latestIndexedRevisionTbl.get(new Long(repoId));
    }

    /**
     * Work out whether a given change, for the specified repository, is already in the index or not.
     */
    private boolean hasDocument(long repoId, long revisionNumber, IndexReader reader) throws IOException
    {
        IndexSearcher searcher = new IndexSearcher(reader);
        try
        {
            TermQuery repoQuery = new TermQuery(new Term(FIELD_REPOSITORY, Long.toString(repoId)));
            TermQuery revQuery = new TermQuery(new Term(FIELD_REVISIONNUMBER, Long.toString(revisionNumber)));
            BooleanQuery repoAndRevQuery = new BooleanQuery();

            repoAndRevQuery.add(repoQuery, BooleanClause.Occur.MUST);
            repoAndRevQuery.add(revQuery, BooleanClause.Occur.MUST);

            TopDocs hits = searcher.search(repoAndRevQuery, MAX_REVISIONS);

            if (hits.totalHits == 1) {
                return true;
            } else if (hits.totalHits == 0) {
                return false;
            } else {
                log.error("Found MORE than one document for revision: " + revisionNumber + ", repository=" + repoId);
                return true;
            }
        }
        finally
        {
            searcher.close();
        }
    }


    // Find all log entries that have already been indexed for the
    // specified repository (i.e. all logs that have been
    // associated with issues in JIRA)
    // Note that this value is not stored in the properties for each repository
    private long updateLastRevisionIndexed(long repoId) throws IndexException, IOException
    {
        log.debug("Updating last revision indexed.");

        long latestIndexedRevision = latestIndexedRevisionTbl.get(repoId);

        Directory dir = FSDirectory.open(new File(getIndexPath()));
        final IndexReader reader;
        try
        {
            reader = IndexReader.open(dir);
        }
        catch (IOException e)
        {
            log.error("Problem with path " + dir.toString() + ": " + e.getMessage(), e);
            throw new IndexException("Problem with path " + dir.toString() + ": " + e.getMessage(), e);
        }
        IndexSearcher searcher = new IndexSearcher(reader);

        try
        {
            TopDocs hits = searcher.search(new TermQuery(new Term(FIELD_REPOSITORY, Long.toString(repoId))), MAX_REVISIONS);
			ScoreDoc[] scoreDoc = hits.scoreDocs;

			for (int i = 0; i < scoreDoc.length; i++) {
				Document doc = searcher.doc(scoreDoc[i].doc);

				final long revision = Long.parseLong(doc.get(FIELD_REVISIONNUMBER));

				if (revision > latestIndexedRevision) {
					latestIndexedRevision = revision;
				}
			}
            log.debug("latestIndRev for " + repoId + " = " + latestIndexedRevision);
            latestIndexedRevisionTbl.put(repoId, latestIndexedRevision);
        }
        finally
        {
            reader.close();
        }

        return latestIndexedRevision;
    }

    /**
     * Creates a new Lucene document for the supplied log entry. This
     * method is used when indexing revisions, not during retrieval.
     *
     * @param repoId   ID of the repository that contains the revision
     * @param logEntry The mercurial log entry that is about to be indexed
     * @return A Lucene document object that is ready to be added to an index
     */
    protected Document getDocument(long repoId, HGLogEntry logEntry)
    {
        Document doc = new Document();
        log.debug("Creating new document for indexing");

        // revision information
        log.debug("FIELD_MESSAGE:" + logEntry.getMessage());
        doc.add(new Field(FIELD_MESSAGE, logEntry.getMessage(), Field.Store.YES, Field.Index.NOT_ANALYZED));

        if (logEntry.getAuthor() != null)
        {
            log.debug("FIELD_AUTHOR:" + logEntry.getAuthor());
            doc.add(new Field(FIELD_AUTHOR, logEntry.getAuthor(), Field.Store.YES, Field.Index.NOT_ANALYZED));
        }

        if (logEntry.getTags() != null)
        {
            String tags = logEntry.getTags();
            log.debug("FIELD_TAGS:" + tags);
            for (String tag: tags.split(",")) {
                doc.add(new Field(FIELD_TAG, tag, Field.Store.YES, Field.Index.NOT_ANALYZED));
            }
        }

        log.debug("FIELD_REPOSITORY:" + repoId);
        doc.add(new Field(FIELD_REPOSITORY, Long.toString(repoId), Field.Store.YES, Field.Index.NOT_ANALYZED));
        log.debug("FIELD_REVISIONNUMBER:" + logEntry.getShortRevision());
        doc.add(new Field(FIELD_REVISIONNUMBER, Long.toString(logEntry.getShortRevision()), Field.Store.YES, Field.Index.NOT_ANALYZED));

        if (logEntry.getDate() != null) {
            // The output looks odd because it is for lucene
            log.debug("FIELD_DATE:" + DateField.dateToString(logEntry.getDate()));
            doc.add(new Field(FIELD_DATE, DateField.dateToString(logEntry.getDate()), Field.Store.YES, Field.Index.NOT_ANALYZED));
        }

        // relevant issue keys
        List<String> keys = getIssueKeysFromString(logEntry);

        // Relevant project keys. Used to avoid adding duplicate
        // projects to the PROJECTKEY field for the same document.
        Map<String, String> projects = new HashMap<String, String>();

        for (String issueKey : keys)
        {
            // That the issue key refers to a currently valid project is
            // explicitly not checked for here to allow for future flexibility.
            // Check for this condition when retrieving documents.
            log.debug("FIELD_ISSUEKEY:" + issueKey);
            doc.add(new Field(FIELD_ISSUEKEY, issueKey, Field.Store.YES, Field.Index.NOT_ANALYZED));
            String projectKey = getProjectKeyFromIssueKey(issueKey);
            if (projectKey != null && !projects.containsKey(projectKey))
            {
                projects.put(projectKey, projectKey);
                log.debug("FIELD_PROJECTKEY:" + projectKey);
                doc.add(new Field(FIELD_PROJECTKEY, projectKey, Field.Store.YES, Field.Index.NOT_ANALYZED));
            }
        }

        return doc;
    }

    protected String getProjectKeyFromIssueKey(String issueKey)
    {
        final String issueKeyUpperCase = StringUtils.upperCase(issueKey);
        // Same as {@link #getProjectKeyFromIssueKey(String)} except that it does not check key validity        
        return JiraKeyUtils.getFastProjectKeyFromIssueKey(issueKeyUpperCase);
    }

    protected List<String> getIssueKeysFromString(HGLogEntry logEntry)
    {
        final String logMessageUpperCase = StringUtils.upperCase(logEntry.getMessage());
        return getIssueKeysFromString(logMessageUpperCase);
    }

    /**
     * We have to be able to distinguish between WTS-123 and WTS-123_1.0
     * The JiraKeysUtil methods don't do that.
     */
    public static List<String> getIssueKeysFromString(String msg)
    {
        // Using JiraKeyUtils.setKeyMatcher() would be neater but the
        // ProductionKeyMatcher class is protected
        List<String> possibleKeys = JiraKeyUtils.getIssueKeysFromString(msg);
        log.debug("String:" + msg + " has possible keys " + possibleKeys);

        // Restrictions are that an issue key in a commit message is
        // always followed by a space, or a period and a space, or a
        // colon, or newline, or comma, or paren, or the end of the string.
        String checkEnd = "[\\s\\+|:|,|)]";
        List<String> result = new ArrayList<String>();
        for (String key: possibleKeys) {
            // It's tedious to get the regex right for these ones
            if (msg.endsWith(key) || msg.indexOf(key + ". ") != -1) {
                result.add(key);
                continue;
            }
            Pattern pattern = Pattern.compile(key + checkEnd);
            Matcher m = pattern.matcher(msg);
            if (m.find()) {
                result.add(key);
                continue;
            }
        }
        log.debug("String:" + msg + " has actual keys " + result);
        return Collections.unmodifiableList(result);
    }

    public Map<Long, List<HGLogEntry>> getLogEntriesByRepository(Issue issue) throws IndexException, IOException
    {
        return getLogEntriesByRepository(issue, 0, Integer.MAX_VALUE, true);
    }

    /**
     * Get the commit entries that refer to the specified issue from all the
     * configured repositories.
     *
     * @param issue      The issue to get entries for.
     * @param startIndex For paging &mdash; The index of the entry that is the first result in the page desired.
     * @param pageSize   For paging &mdash; The size of the page.
     * @return A {@link java.util.Map} of
     * {@net.customware.jira.plugins.ext.mercurial.MercurialManager}
     * IDs to the commit entries in them that relate to the issue.
     *
     * @throws IndexException Thrown if there's a getting a reader to the index.
     * @throws IOException    Thrown if there's a problem reading the index.
     */
    public Map<Long, List<HGLogEntry>> getLogEntriesByRepository(Issue issue, int startIndex, int pageSize, boolean ascending) throws IndexException, IOException
    {
        log.debug("Retrieving revisions for : " + issue.getKey());

        if (!indexDirectoryExists()) {
            log.warn("The indexes for the mercurial plugin have not yet been created.");
            return null;
        }
        
        final IndexReader reader = indexAccessor.getIndexReader(getIndexPath());
        IndexSearcher searcher = new IndexSearcher(reader);
        
        try {
            TopDocs hits = searcher.search(createQueryByIssueKey(issue), MAX_REVISIONS, new Sort(new SortField(FIELD_DATE, SortField.STRING, !ascending)));
            ScoreDoc[] scoreDoc = hits.scoreDocs;

            Map<Long, List<HGLogEntry>> logEntries = new LinkedHashMap<Long, List<HGLogEntry>>(scoreDoc.length);
            
            int endIndex = startIndex + pageSize;
            
            for (int i = 0; i < scoreDoc.length; i++) {
                if (i < startIndex || i >= endIndex) {
                    continue;
                }
                
                Document doc = searcher.doc(scoreDoc[i].doc);
                // repositoryId is UUID + location
                long repositoryId = Long.parseLong(doc.get(FIELD_REPOSITORY)); 
                MercurialManager manager = multipleMercurialRepositoryManager.getRepository(repositoryId);
                if (manager == null) {
                    // A stale index refering to a repository that is
                    // no longer defined
                    // TODO remove Document doc from the index
                    log.warn("No mercurial repository manager found for repository id: " + repositoryId);
                    continue;
                }
                
                long revision = Long.parseLong(doc.get(FIELD_REVISIONNUMBER));
                HGLogEntry logEntry = manager.getLogEntry(revision);
                if (logEntry == null) {
                    log.error("Could not find log message for revision " + Long.parseLong(doc.get(FIELD_REVISIONNUMBER)));
                    continue;
                }
                // Look for list of map entries for repository
                List<HGLogEntry> entries = logEntries.get(repositoryId);
                if (entries == null) {
                    entries = new ArrayList<HGLogEntry>();
                    logEntries.put(repositoryId, entries);
                }
                entries.add(logEntry);
            }
            
            return logEntries;
        } finally {
            searcher.close();
            reader.close();
        }
    }

    /**
     * Get the commit entries that refer to the specified project.
     *
     * @param projectKey The project key.
     * @param user       The requesting user.
     * @param startIndex For paging &mdash; The index of the entry that is the first result in the page desired.
     * @param pageSize   For paging &mdash; The size of the page.
     * @return A {@link java.util.Map} of
     * {@net.customware.jira.plugins.ext.mercurial.MercurialManager}
     * IDs to the commit entries in them that relate to the issue.
     *
     * @throws IndexException Thrown if there's a getting a reader to the index.
     * @throws IOException    Thrown if there's a problem reading the index.
     */
    public Map<Long, List<HGLogEntry>> getLogEntriesByProject(String projectKey, User user, int startIndex, int pageSize) throws IndexException, IOException
    {
        if (!indexDirectoryExists()) {
            log.warn("getLogEntriesByProject() The indexes for the mercurial plugin have not yet been created.");
            return null;
        }

        // Set up and perform a search for all documents having the
        // supplied projectKey, sorted in descending date order
        TermQuery query = new TermQuery(new Term(FIELD_PROJECTKEY, projectKey));

        Map<Long, List<HGLogEntry>> logEntries;
        final IndexReader reader = indexAccessor.getIndexReader(getIndexPath());
        IndexSearcher searcher = new IndexSearcher(reader);

        try {
            TopDocs hits = searcher.search(query, new ProjectRevisionFilter(issueManager, permissionManager, user, projectKey), MAX_REVISIONS, new Sort(new SortField(FIELD_DATE, SortField.STRING, true)));
            ScoreDoc[] scoreDoc = hits.scoreDocs;
            
            // Build the result map
            logEntries = new LinkedHashMap<Long, List<HGLogEntry>>();
            int endIndex = startIndex + pageSize;
            
            for (int i = 0, j = scoreDoc.length; i < j; ++i) {
                if (i < startIndex || i >= endIndex) {
                    continue;
                }
                
				Document doc = searcher.doc(scoreDoc[i].doc);
                long repositoryId = Long.parseLong(doc.get(FIELD_REPOSITORY));//repositoryId is UUID + location
                MercurialManager manager = multipleMercurialRepositoryManager.getRepository(repositoryId);
                long revision = Long.parseLong(doc.get(FIELD_REVISIONNUMBER));
                HGLogEntry logEntry = manager.getLogEntry(revision);
                if (logEntry == null) {
                    log.error("getLogEntriesByProject() Could not find log message for revision: " + revision);
                    continue;
                }
                // Look up the list of map entries for this
                // repository. Create one if needed.
                List<HGLogEntry> entries = logEntries.get(repositoryId);
                if (entries == null) {
                    entries = new ArrayList<HGLogEntry>();
                    logEntries.put(repositoryId, entries);
                }
                entries.add(logEntry);
            }
        } finally {
            searcher.close();
            reader.close();
        }
        
        return logEntries;
    }


    /**
     * Gets all commits for issues related to version specified from
     * all configured repositories.
     *
     * @param version    The version to get entries for. May not be <tt>null</tt>.
     * @param user       The requesting user.
     * @param startIndex For paging &mdash; The index of the entry that is the first result in the page desired.
     * @param pageSize   For paging &mdash; The size of the page.
     * @return A {@link java.util.Map} of
     * {@net.customware.jira.plugins.ext.mercurial.MercurialManager}
     * IDs to the commit entries in them that relate to the issue.
     *
     * @throws IndexException Thrown if there's a getting a reader to the index.
     * @throws IOException    Thrown if there's a problem reading the index.
     */
    public Map<Long, List<HGLogEntry>> getLogEntriesByVersion(Version version, User user, int startIndex, int pageSize) throws IndexException, IOException
    {
        if (!indexDirectoryExists()) {
            log.warn("getLogEntriesByVersion() The indexes for the mercurial plugin have not yet been created.");
            return null;
        }

        // Find all isuses affected by and fixed by any of the versions:
        Collection<GenericValue> issues = new HashSet<GenericValue>();

        try {
            issues.addAll(versionManager.getFixIssues(version));
            issues.addAll(versionManager.getAffectsIssues(version));
        } catch (Exception e) {
            log.error("getLogEntriesByVersion() Caught exception while looking up issues related to version " + version.getName() + "!", e);
            // Keep going. We may have got some issues stored.
        }

        // Construct a query with all the issue keys. Make sure to
        // increase the maximum number of clauses if needed.
        int maxClauses = BooleanQuery.getMaxClauseCount();
        if (issues.size() > maxClauses) {
            BooleanQuery.setMaxClauseCount(issues.size());
        }
        BooleanQuery query = new BooleanQuery();
        Set<String> permittedIssueKeys = new HashSet<String>();

        for (GenericValue issue : issues) {
            String key = issue.getString(FIELD_ISSUEKEY);
            Issue theIssue = issueManager.getIssueObject(key);
            
            if (permissionManager.hasPermission(Permissions.VIEW_VERSION_CONTROL, theIssue, user)) {
                TermQuery termQuery = new TermQuery(new Term(FIELD_ISSUEKEY, key));
                query.add(termQuery, BooleanClause.Occur.SHOULD);
                permittedIssueKeys.add(key);
            }
        }

        final IndexReader reader = indexAccessor.getIndexReader(getIndexPath());
        IndexSearcher searcher = new IndexSearcher(reader);
        Map<Long, List<HGLogEntry>> logEntries;

        try {
            // Run the query and sort by date in descending order
            TopDocs hits = searcher.search(query, new PermittedIssuesRevisionFilter(issueManager, permissionManager, user, permittedIssueKeys), MAX_REVISIONS, new Sort(new SortField(FIELD_DATE, SortField.STRING, true)));
			ScoreDoc[] scoreDoc = hits.scoreDocs;
			
            logEntries = new LinkedHashMap<Long, List<HGLogEntry>>();
            int endDocIndex = startIndex + pageSize;

            for (int i = 0, j = scoreDoc.length; i < j; ++i) {
                if (i < startIndex || i >= endDocIndex) {
                    continue;
                }
                
				Document doc = searcher.doc(scoreDoc[i].doc);
                // repositoryId is UUID + location
                long repositoryId = Long.parseLong(doc.get(FIELD_REPOSITORY));
                MercurialManager manager = multipleMercurialRepositoryManager.getRepository(repositoryId);
                long revision = Long.parseLong(doc.get(FIELD_REVISIONNUMBER));

                HGLogEntry logEntry = manager.getLogEntry(revision);
                if (logEntry == null) {
                    log.error("getLogEntriesByVersion() Could not find log message for revision: " + Long.parseLong(doc.get(FIELD_REVISIONNUMBER)));
                }
                // Add the entry to the list of map entries for the
                // repository. Create a new list if needed.
                List<HGLogEntry> entries = logEntries.get(repositoryId);
                if (entries == null) {
                    entries = new ArrayList<HGLogEntry>();
                    logEntries.put(repositoryId, entries);
                }
                entries.add(logEntry);
            }
        }
        finally {
            searcher.close();
            reader.close();
            BooleanQuery.setMaxClauseCount(maxClauses);
        }

        return logEntries;
    }

    /**
     * For the given revision in a given repository, find all issues
     * in the indexed document for that commit.  The issues are not
     * filtered for reference to valid projects.
     *
     * @param manager The given repository manager
     * @param revision The given revision
     *
     * @return a list of the issues referred to by the given revision
     */
    public Set<String> getIssueKeysByRevision(MercurialManager manager, long revision) throws IndexException, IOException {
        if (!indexDirectoryExists()) {
            log.warn("getIssueKeysByRevision() The indexes for the mercurial plugin have not yet been created.");
            return null;
        }

        String repoName = "'" + manager.getDisplayName() + "'";
        
        final IndexReader reader = indexAccessor.getIndexReader(getIndexPath());
        IndexSearcher searcher = new IndexSearcher(reader);

        // TODO add permission checking for each issue?
        TermQuery repoQuery = new TermQuery(new Term(FIELD_REPOSITORY, Long.toString(manager.getId())));
        TermQuery revQuery = new TermQuery(new Term(FIELD_REVISIONNUMBER, Long.toString(revision)));
        BooleanQuery repoAndRevQuery = new BooleanQuery();
        repoAndRevQuery.add(repoQuery, BooleanClause.Occur.MUST);
        repoAndRevQuery.add(revQuery, BooleanClause.Occur.MUST);

        TopDocs hits = searcher.search(repoAndRevQuery, MAX_REVISIONS);
        if (hits.totalHits == 0) {
            log.debug("getIssueKeysByRevision() No matches -- returning null.");
            // Need to close the searcher and the reader to prevent running
            // out of open file handles
            searcher.close();
            reader.close();
            return null;
        }

        // When the document was created for an hg log entry, multiple
        // issue keys were added to it. One or more project key was added to
        // it and one revision value.
        if (hits.totalHits != 1) {
            log.error("getIssueKeysByRevision() More than one Lucene document was found for repository " + repoName + " and revision " + revision);
            searcher.close();
            reader.close();
            return null;
        }
        
        Set<String> issueKeys = new HashSet<String>();
        Document doc = searcher.doc(hits.scoreDocs[0].doc);
        if (doc == null) {
            log.error("getIssueKeysByRevision() null document was found for repository " + repoName + " and revision " + revision);
            searcher.close();
            reader.close();
            return null;
        }

        // The Lucene project key value should exist even if it isn't a
        // current JIRA project. That's only true if the revision had
        // issue keys and thus one or more projects
        if (requireValidProject) {
            boolean foundValidProject = requireOneValidProject(doc);
            if (!foundValidProject) {
                log.debug("Ignoring revision " + revision + " since no issue in a current JIRA project was found");
                searcher.close();
                reader.close();
                return null;
            }
        }

        String[] keys = doc.getValues(FIELD_ISSUEKEY);
        for (String key: keys) {
            issueKeys.add(key);
        }
        
        searcher.close();
        reader.close();

        return issueKeys;
    }            

    /**
     * Find the users that committed changeset that refer to the given issue.
     * The authors are not filtered for reference to valid users or projects.
     *
     * @return a list of the userids referred to by the given issue key
     */
    public Set<String> getAuthorsByIssue(String issueKey) throws IndexException, IOException {
        if (!indexDirectoryExists()) {
            log.warn("getAuthorsByIssue() The indexes for the mercurial plugin have not yet been created.");
            return null;
        }
        
        final IndexReader reader = indexAccessor.getIndexReader(getIndexPath());
        IndexSearcher searcher = new IndexSearcher(reader);

        // TODO add permission checking for each issue?
        TermQuery query = new TermQuery(new Term(FIELD_ISSUEKEY, issueKey));
        TopDocs hits = searcher.search(query, MAX_REVISIONS);
        ScoreDoc[] scoreDoc = hits.scoreDocs;

        Set<String> authors = new HashSet<String>();
        for (int i=0; i<scoreDoc.length; i++) {
            Document doc = searcher.doc(scoreDoc[i].doc);
            String author = doc.get(FIELD_AUTHOR);
            
            if (author == null) {
                log.debug("No author was found for the current changeset");
                continue;
            }
            if (requireValidProject) {
                boolean foundValidProject = requireOneValidProject(doc);
                if (!foundValidProject) {
                    log.debug("Ignoring author " + author + " since no valid project was found");
                    continue;
                }
            }
            authors.add(author);
        }
		
        searcher.close();
        reader.close();
        return authors;
    }            


    /**
     * Find the revision number of the given tag or -1 if the tag
     * does not exist.
     */
    public long getRevisionForTag(MercurialManager manager, String tag) throws IndexException, IOException {
        long result = -1;
        if (!indexDirectoryExists()) {
            log.warn("getRevisionForTag(): The indexes for the mercurial plugin have not yet been created.");
            return result;
        }
        
        final IndexReader reader = indexAccessor.getIndexReader(getIndexPath());
        IndexSearcher searcher = new IndexSearcher(reader);
        
        // TODO add permission checking for each issue?
        TermQuery repoQuery = new TermQuery(new Term(FIELD_REPOSITORY, Long.toString(manager.getId())));
        TermQuery tagQuery = new TermQuery(new Term(FIELD_TAG, tag));
        BooleanQuery repoAndTagQuery = new BooleanQuery();
        repoAndTagQuery.add(repoQuery, BooleanClause.Occur.MUST);
        repoAndTagQuery.add(tagQuery, BooleanClause.Occur.MUST);
        
        TopDocs hits = searcher.search(repoAndTagQuery, MAX_REVISIONS);
        if (hits.totalHits == 0) {
            log.debug("getRevisionForTag(): tag not found: " + tag);
        } else if (hits.totalHits > 1) {
            log.warn("getRevisionForTag(): " + hits.totalHits + " hits found for the tag " + tag + ", expected 0 or 1");
        } else {
            Document doc = searcher.doc(hits.scoreDocs[0].doc);
            String revision = doc.get(FIELD_REVISIONNUMBER);
            if (revision == null) {
                log.warn("No revision number was found for the changeset indexed with tag " + tag);
            } else {
                try {
                    log.debug("Found revision " + revision + " in repo " + manager.getId() + " for tag " + tag);
                    result = Long.parseLong(revision);
                } catch (NumberFormatException nfe) {
                    log.error("Non-numeric revision found: " + nfe.getMessage());
                    result = -1;
                }
            }
        }
		
        searcher.close();
        reader.close();

        return result;
    }            


    /**
     * Return true if at least one project referred to by the given Document 
     * is a currently valid JIRA project.
     */
    private boolean requireOneValidProject(Document doc) {
        String[] projectKeys = doc.getValues(FIELD_PROJECTKEY);
        boolean foundValidProject = false;
        for (String projectKey: projectKeys) {
            if (projectManager.getProjectObjByKey(projectKey) != null) {
                foundValidProject = true;
            }
        }
        return foundValidProject;
    }

    /**
     * Return a map of changeset to commit message for the given repository ID.
     * The messages are not filtered for reference to valid projects.
     * Since this method is called from a constructor that shouldn't raise 
     * an exception, it doesn't throw any exceptions.
     */
    public Map<Long, String> getMessagesByRepo(long repoId) {
        if (!indexDirectoryExists()) {
            log.warn("getMessagesByRepo() The indexes for the mercurial plugin have not yet been created.");
            return null;
        }
        log.debug("getMessagesByRepo repoId: " + repoId);
        Map result = new TreeMap<Long, String>(ReverseLongComparator.COMPARATOR);
        IndexReader reader = null;
        IndexSearcher searcher = null;
        try {
            reader = indexAccessor.getIndexReader(getIndexPath());
            searcher = new IndexSearcher(reader);
        } catch (IndexException ie) {
            log.error(ie.getMessage());
            return null;
        }

        try {            
            TermQuery query = new TermQuery(new Term(FIELD_REPOSITORY, Long.toString(repoId)));
            TopDocs hits = searcher.search(query, MAX_REVISIONS);
            ScoreDoc[] scoreDoc = hits.scoreDocs;
            
            for (int i=0; i<scoreDoc.length; i++) {
                Document doc = searcher.doc(scoreDoc[i].doc);
                String message = doc.get(FIELD_MESSAGE);
                Long changeset = new Long(doc.get(FIELD_REVISIONNUMBER));
                if (requireValidProject) {
                    boolean foundValidProject = requireOneValidProject(doc);
                    if (!foundValidProject) {
                        log.debug("Ignoring changeset " + changeset + " since no valid project was found");
                        continue;
                    }
                }
                log.debug("getMessagesByRepo: adding changeset " + changeset + ": START|" + message + "|END");
                result.put(changeset, message);
            }
        } catch (IOException io) {
            log.error(io.getMessage());
            return null;
        } finally {
            try {
                searcher.close();
                reader.close();
            } catch (IOException io) {
                log.error(io.getMessage());
                return null;
            }
        } 

        return result;
    }            

    public void addRepository(MercurialManager mercurialManager)
    {
        // TODO is this causing a second invocation of hg pull?
        log.warn("Adding a new repository " + mercurialManager.getDisplayName());
        initializeLatestIndexedRevisionCache(mercurialManager);
        try
        {
            // This no longer updates all the repositories, just the added one
            checkAndUpdateOneIndex(mercurialManager);
        }
        catch (Exception e)
        {
            throw new InfrastructureException("Could not index repository", e);
        }
    }

    public void removeEntries(MercurialManager mercurialInstance) throws IOException, IndexException, HGException
    {
        if (log.isDebugEnabled())
        {
            log.debug("Deleting revisions for : " + mercurialInstance.getRoot());
        }

        if (!indexDirectoryExists())
        {
            log.warn("The indexes for the mercurial plugin have not yet been created.");
        }
        else
        {
            long repoId = mercurialInstance.getId();

            IndexWriter writer = null;

            try
            {
                writer = indexAccessor.getIndexWriter(getIndexPath(), false, ANALYZER);

                writer.deleteDocuments(new Term(FIELD_REPOSITORY, Long.toString(repoId)));
                initializeLatestIndexedRevisionCache(mercurialInstance);
            }
            catch (IndexException ie)
            {
                if (log.isEnabledFor(Level.ERROR))
                    log.error("Unable to open index. " +
                            "Perhaps the index is corrupted. It might be possible to fix the problem " +
                            "by removing the index directory (" + getIndexPath() + ")", ie);

                throw ie; /* Rethrow for normal error handling? issue SVN-200 */
            }
            finally
            {
                if (null != writer)
                {
                    try
                    {
                        writer.close();
                    }
                    catch (IOException ioe)
                    {
                        if (log.isEnabledFor(Level.WARN))
                            log.warn("Unable to close index.", ioe);
                    }
                }

            }
        }
    }

    /**
     * Returns the query that matches the key of the passed issue and
     * any previous keys this issue had if it has moved between
     * projects previously.
     */
    protected Query createQueryByIssueKey(Issue issue)
    {
        BooleanQuery query = new BooleanQuery();

        // add current key
        query.add(new TermQuery(new Term(FIELD_ISSUEKEY, issue.getKey())), BooleanClause.Occur.SHOULD);

        // add all previous keys
        Collection<String> previousIssueKeys = changeHistoryManager.getPreviousIssueKeys(issue.getId());
        for (String previousIssueKey : previousIssueKeys)
        {
            TermQuery termQuery = new TermQuery(new Term(FIELD_ISSUEKEY, previousIssueKey));
            query.add(termQuery, BooleanClause.Occur.SHOULD);
        }

        return query;
    }

}
