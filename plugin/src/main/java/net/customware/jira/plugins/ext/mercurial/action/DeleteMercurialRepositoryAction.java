package net.customware.jira.plugins.ext.mercurial.action;

import net.customware.jira.plugins.ext.mercurial.MercurialManager;

public class DeleteMercurialRepositoryAction extends MercurialActionSupport {
	private long repoId;
	private MercurialManager mercurialManager;

	public DeleteMercurialRepositoryAction() {
	}

	public String getRepoId() {
		return Long.toString(repoId);
	}

	public void setRepoId(String repoId) {
		this.repoId = Long.parseLong(repoId);
	}

	public String doDefault() {
        if (!hasPermissions())
        {
            return PERMISSION_VIOLATION_RESULT;
        }

		mercurialManager = getMultipleRepoManager().getRepository(repoId);
		return INPUT;
	}

	public String doExecute() {
        if (!hasPermissions())
        {
            return PERMISSION_VIOLATION_RESULT;
        }

		getMultipleRepoManager().removeRepository(repoId);
		return getRedirect("ViewMercurialRepositories.jspa");
	}

	public MercurialManager getMercurialManager() {
		return mercurialManager;
	}
}
