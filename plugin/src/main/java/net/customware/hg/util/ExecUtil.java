package net.customware.hg.util;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.lang.Runtime;
import java.lang.Process;
import org.apache.log4j.Logger;

public class ExecUtil {
    public ExecUtil() {
    }

    /**
     * Call Runtime.exec() and capture stdout and stderr,
     */
    public ArrayList<String> exec(String cmd, List<String> env_vars, File curr_dir, Logger log) throws IOException {
    ArrayList<String> result = new ArrayList<String>();
	if (env_vars == null)
	{
		env_vars = new ArrayList<String>();
	}
	env_vars.add("LANG=C");
    
    try {
	if (curr_dir != null) {
	    log.debug("Current directory: " + curr_dir.getAbsolutePath());
	}
        
	// added to print current directory and the vars that are passed to Runtime class
        String vars = "" ;
        for (int i = 0; i< env_vars.size(); i++) {
            vars += "\t" + env_vars.get(i);
        }
        log.info("Running: " + cmd + " with env vars " + vars + " in directory: " + curr_dir);

	//JHG-26 - Error adding mercurial repository using http protocol
	Process p = Runtime.getRuntime().exec(cmd, env_vars.toArray(new String[env_vars.size()]), curr_dir);
	StreamGobbler errorGobbler = new StreamGobbler(p.getErrorStream(), "ERROR", null, log);
	StreamGobbler outputGobbler = new StreamGobbler(p.getInputStream(), "OUTPUT", result, log);
	errorGobbler.start();
	outputGobbler.start();
	
	if (p.waitFor() != 0) {
	    // Wait for the threads to finish reading
	    outputGobbler.join();
	    errorGobbler.join();
	    // Close streams
	    p.getErrorStream().close();
	    p.getInputStream().close();
            // TODO add the error output to the message
            // e.g. pulling from http://localhost:8000/test
            // searching for changes
            // abort: repository is unrelated
	    throw new IOException("Non-zero return value from command " + cmd + " executed in directory " + curr_dir);
	}
	// Wait for the threads to finish reading
	outputGobbler.join();
	errorGobbler.join();
	// Close streams
	p.getErrorStream().close();
	p.getInputStream().close();
    } catch (InterruptedException ie) {
	log.info("command interrupted");
    }
    return result;
}

/**
 * Handle all of the output from an exec call.
 */
class StreamGobbler extends Thread
{
    InputStream is;
    String type;
    ArrayList<String> lines;
    Logger log;

    StreamGobbler(InputStream is, String type, ArrayList<String> linesList, Logger logger)
    {
	this.is = is;
	this.type = type;
	this.lines = linesList;
	this.log = logger;
    }
    
    public void run()
    {
	try
	    {
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String line=null;
		while ( (line = br.readLine()) != null) {
		    if (type.equals("OUTPUT")) {
			lines.add(line);
		    } else {
			log.debug(type + ": " + line); // This is an error from running the command
		    }
		}
	    } catch (IOException ioe)
	    {
		ioe.printStackTrace();  
	    }
    }
}

}
